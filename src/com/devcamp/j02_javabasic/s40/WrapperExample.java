package com.devcamp.j02_javabasic.s40;

public class WrapperExample {
    public static void autoBoxing() {
        byte bte = 100;
        short sh = 200;
        int it = 300;
        long lng = 400;
        float fat = 500.0f;
        double dlb = 600.0D;
        char ch = 'b';
        boolean boo = false;
        /*
         * Autoboxing: converting primitives into objects
         * autoboxing là cơ chế tự động chuyển đổi kiểu dữ liệu nguyên thủy sang object của wrapper class tương ứng.
         */
        Byte byteobj = bte;
        Short shortobj = sh;
        Integer intobj = it;
        Long longobj = lng;
        Float floatobj = fat;
        Double doubleobj = dlb;
        Character chartobj = ch;
        Boolean boolobj = boo;

        System.out.println("---Printing object value (In giá trị của object)---");
        System.out.println("Byte object: " + byteobj);
        System.out.println("Short object: " + shortobj);
        System.out.println("Integer object: " + intobj);
        System.out.println("Long object: " + longobj);
        System.out.println("Float object: " + floatobj);
        System.out.println("Double object: " + doubleobj);
        System.out.println("Character object: " + chartobj);
        System.out.println("Boolean object: " + boolobj);
    }

    public static void unBoxing(){
        byte bte = 11;
        short sh = 21;
        int it = 31;
        long lng = 41;
        float fat = 51.0f;
        double dlb = 61.0D;
        char ch = 'b';
        boolean boo = false;
        /*
         * Autoboxing: converting primitives into objects
         * autoboxing là cơ chế tự động chuyển đổi kiểu dữ liệu nguyên thủy sang object của wrapper class tương ứng.
         */
        Byte byteobj = bte;
        Short shortobj = sh;
        Integer intobj = it;
        Long longobj = lng;
        Float floatobj = fat;
        Double doubleobj = dlb;
        Character chartobj = ch;
        Boolean boolobj = boo;
        /*
         * Unboxing : converting Objects to Primitives
         * Unboxing là cơ chế tự động chuyển đổi các object của wrapper class sang kiểu dữ liệu nguyên thủy tương ứng
         */

         byte bytevalue = byteobj;
         short shortvalue = shortobj;
         int intvalue = intobj;
         long longvalue = longobj;
         float floatvalue = floatobj;
         double doublevalue = doubleobj;
         char chartvalue = chartobj;
         boolean boovalue = boolobj;

        System.out.println("--Printing primitives value (In giá tri cua các Primitives DataType (Kieu du lieu nguyên thuy)---");
        System.out.println("byte value: " + bytevalue);
        System.out.println("short value: " + shortvalue);
        System.out.println("int value: " + intvalue);
        System.out.println("long value: " + longvalue);
        System.out.println("float value: " + floatvalue);
        System.out.println("double value: " + doublevalue);
        System.out.println("char value: " + chartvalue);
        System.out.println("boolean value: " + boovalue);
    }

    public static void main(String[] args) {
        WrapperExample.autoBoxing();
        WrapperExample.unBoxing();
    }
}
